package com.example.askhat.training;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Algorithms extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_algorithms);
    }

    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        startActivity(intent);
    }

    public void breadthFirstSearch(View view) {
        Intent intent = new Intent(this, DisplayBFSActivity.class);
        startActivity(intent);
    }
    public void linearSearch(View view) {
        Intent intent = new Intent(this, DisplayLinearSearchActivity.class);
        startActivity(intent);
    }
    public void twoPointer(View view) {
        Intent intent = new Intent(this, TwoPointerTechnique.class);
        startActivity(intent);
    }
    public void binarySearch(View view) {
        Intent intent = new Intent(this, BinarySearch.class);
        startActivity(intent);
    }

}
