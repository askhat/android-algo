package com.example.askhat.training;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;


public class DisplayBFSActivity extends AppCompatActivity {
    private int cnt = 0;
    private int[] allSteps = {1, 2, 4, 5, 3, 6, 7};
    private int index = 0;
    private Map<String, ImageView> nodes;
    private Map<String, ImageView> edges;
    private TextView txt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        initialize();
        ButtonClick();
    }

    public void ButtonClick() {


        Button buttonsbm = (Button)findViewById(R.id.bfsButton);
        buttonsbm.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(index < allSteps.length) {
                            int curValue = allSteps[index];
                            ImageView curNode = nodes.get("node" + curValue);
                            if(curNode != null) {
                                curNode.setImageResource(android.R.drawable.star_big_on);
                            }
                            for(int i = 0; i <= 1; i++) {
                                ImageView curEdge = edges.get("edge" + curValue + "" + (2 * curValue + i));
                                if(curEdge != null) {
                                    curEdge.setImageResource(android.R.drawable.button_onoff_indicator_on);
                                }
                            }


                            if(cnt == 0) txt.setText(getString(R.string.bfs_menu));

                            cnt++;
                            index++;
                        }
                    }
                }
        );

    }

    public void initialize() {
        txt = (TextView)findViewById(R.id.textView);

        nodes = new HashMap<>();
        edges = new HashMap<>();

        nodes.put("node1", (ImageView)findViewById(R.id.node1));
        nodes.put("node2", (ImageView)findViewById(R.id.node2));
        nodes.put("node3", (ImageView)findViewById(R.id.node3));
        nodes.put("node4", (ImageView)findViewById(R.id.node4));
        nodes.put("node5", (ImageView)findViewById(R.id.node5));
        nodes.put("node6", (ImageView)findViewById(R.id.node6));
        nodes.put("node7", (ImageView)findViewById(R.id.node7));


        edges.put("edge12", (ImageView)findViewById(R.id.edge12));
        edges.put("edge13", (ImageView)findViewById(R.id.edge13));
        edges.put("edge24", (ImageView)findViewById(R.id.edge24));
        edges.put("edge25", (ImageView)findViewById(R.id.edge25));
        edges.put("edge36", (ImageView)findViewById(R.id.edge36));
        edges.put("edge37", (ImageView)findViewById(R.id.edge37));
    }
}
