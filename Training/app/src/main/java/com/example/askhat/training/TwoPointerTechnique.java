package com.example.askhat.training;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class TwoPointerTechnique extends AppCompatActivity {


    private int cnt = 0;
    private int changeX = 800;
    private ImageView rightImg;
    private ImageView leftImg;

    private TextView txt;
    private TextView topText;
    private TextView congrats;

    private EditText number1;
    private EditText number2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_pointer_technique);

        ButtonClick();
    }

    public void ButtonClick() {
        final ImageView rightImg = (ImageView)findViewById(R.id.rightArrow);
        final ImageView leftImg = (ImageView)findViewById(R.id.leftArrow);

        final TextView txt = (TextView)findViewById(R.id.lstext);
        final TextView topText = (TextView)findViewById(R.id.textView2);
        final TextView congrats = (TextView)findViewById(R.id.congrats);

        final EditText number1 = (EditText)findViewById(R.id.findValue);
        final EditText number2 = (EditText)findViewById(R.id.editText3);

        Button buttonsbm = (Button)findViewById(R.id.binNext);



        buttonsbm.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(cnt == 0) {
                            txt.setText(getString(R.string.pointer));
                        } else if(cnt == 1) {
                            txt.setText(getString(R.string.p1));
                        } else if(cnt == 2) {
                            txt.setText(getString(R.string.p2));
                        }

                        if(cnt < 2) {
                            rightImg.setX(rightImg.getX() - 175);
                        } else if(cnt < 3) {

                            leftImg.setX(leftImg.getX() + 175);
                            topText.setVisibility(View.INVISIBLE);
                            congrats.setTextColor(Color.GREEN);
                            congrats.setVisibility(View.VISIBLE);
                            number1.setBackgroundColor(Color.GREEN);
                            number2.setBackgroundColor(Color.GREEN);

                        }
                        if(cnt == 0) {
                            txt.setText(getString(R.string.p0));
                        }
                        cnt++;
                    }
                }
        );
    }

}
