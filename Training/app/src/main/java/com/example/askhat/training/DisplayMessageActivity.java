package com.example.askhat.training;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;


public class DisplayMessageActivity extends AppCompatActivity {
    private int cnt = 0;
    private int[] allSteps = {1, 2, 4, 5, 3, 6, 7};
    private int[] traverseEdge = {0, 0, 0, 0, 0, 0, 0};
    private boolean[] used = {false, false, false, false, false, false, false};
    private int[] parent = {1, 1, 1, 2, 2, 3, 3};
    private int index = 0;
    private boolean flag = false;
    private Map<String, ImageView> edges;
    private Map<String, ImageView> nodes;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        initialize();
        ButtonClick();
    }
    private void showToast(int duration, final String message) {
        final Toast toast = Toast.makeText(getBaseContext(),
                message,
                Toast.LENGTH_SHORT);
        toast.show();
        new CountDownTimer(duration, 500) {
            public void onTick(long millisUntilFinished) {
                toast.show();showToast(500, message);
            }
            public void onFinish() {
                toast.cancel();
            }

        }.start();
    }
    public void initialize() {
        nodes = new HashMap<>();
        edges = new HashMap<>();

        nodes.put("node1", (ImageView)findViewById(R.id.node1));
        nodes.put("node2", (ImageView)findViewById(R.id.node2));
        nodes.put("node3", (ImageView)findViewById(R.id.node3));
        nodes.put("node4", (ImageView)findViewById(R.id.node4));
        nodes.put("node5", (ImageView)findViewById(R.id.node5));
        nodes.put("node6", (ImageView)findViewById(R.id.node6));
        nodes.put("node7", (ImageView)findViewById(R.id.node7));



        edges.put("edge12", (ImageView)findViewById(R.id.edge12));
        edges.put("edge13", (ImageView)findViewById(R.id.edge13));
        edges.put("edge24", (ImageView)findViewById(R.id.edge24));
        edges.put("edge25", (ImageView)findViewById(R.id.edge25));
        edges.put("edge36", (ImageView)findViewById(R.id.edge36));
        edges.put("edge37", (ImageView)findViewById(R.id.edge37));
    }

    public void ButtonClick() {

        final ImageView imgView = (ImageView)findViewById(R.id.node1);
        final TextView txt = (TextView)findViewById(R.id.textView);

        Button buttonsbm = (Button)findViewById(R.id.bfsButton);
        buttonsbm.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//
                        String menuText = getString(R.string.menu_text);
                        if(cnt == 0) {
                            txt.setText(menuText);
                        }

                        cnt++;
                        int curValue = allSteps[index];
                        ImageView curNode = nodes.get("node" + curValue);

                        if(curNode != null && used[index] && traverseEdge[index] >= 2) {

                            if(!flag) {
                                showToast(500, "Visiting parent...");
                                flag = true;
                            } else {
                                showToast(500, "You traversed the graph!");
                            }

                            index /= 2;
                            return;
                        }
                        if(curNode != null) {
                            if(!used[index]) {
                                curNode.setImageResource(android.R.drawable.star_big_on);
                                used[index] = true;
//                                index++;
                                return;
                            } else if(traverseEdge[index] == 0) {
                                ImageView curEdge = edges.get("edge" + curValue + ("" + (2 * curValue)));
                                if(curEdge != null) {
                                    curEdge.setImageResource(android.R.drawable.button_onoff_indicator_on);
                                    traverseEdge[index]++;
                                    index++;
                                    return;
                                }
                                else {
                                    showToast(500, "Visiting parent...");

                                    index /= 2;
                                    if(index == 2) {
                                        index = 4;
                                    }
                                    return;
                                }
                            } else if(traverseEdge[index] == 1) {
                                ImageView curEdge = edges.get("edge" + curValue + ("" + (2 * curValue + 1)));

                                if(curEdge != null) {
                                    curEdge.setImageResource(android.R.drawable.button_onoff_indicator_on);
                                    traverseEdge[index]++;
                                    int nextValue = 2 * curValue + 1;
                                    System.out.println("Next value is " + nextValue);
                                    for(int i = 0; i < allSteps.length; i++) {
                                        if(allSteps[i] == nextValue) {
                                            index = i;
                                            break;
                                        }
                                    }
                                    return;
                                }
                            }
                        }

                    }
                }
        );

    }
}
