package com.example.askhat.training;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Congrats extends AppCompatActivity {

    private static int mx = -100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congrats);

        TextView txt = (TextView)findViewById(R.id.mytext);
        TextView botTxt = (TextView)findViewById(R.id.bot);
        TestKnowledge test = new TestKnowledge();

        txt.setText("Your current score is " + test.score);
        botTxt.setText("Your maximum score is " + Math.max(test.score, mx));

        mx = Math.max(test.score, mx);
        test.score = 0;
    }
}
