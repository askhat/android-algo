package com.example.askhat.training;

import android.graphics.Color;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BinarySearch extends AppCompatActivity {


    private int cnt = 0;
    private boolean flag = false;
    List<TextView> numberBoxes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binary_search);
        initialize();
        Colorize();
        ButtonClick();
    }

    public void initialize() {
        numberBoxes = new ArrayList<>();
        numberBoxes.add((TextView)findViewById(R.id.n1));
        numberBoxes.add((TextView)findViewById(R.id.n2));
        numberBoxes.add((TextView)findViewById(R.id.n3));
        numberBoxes.add((TextView)findViewById(R.id.n4));
        numberBoxes.add((TextView)findViewById(R.id.n5));
        numberBoxes.add((TextView)findViewById(R.id.n6));
        numberBoxes.add((TextView)findViewById(R.id.n7));

    }

    public void Colorize() {
        Random rnd = new Random();
        for(int i = 0; i < numberBoxes.size(); i++) {
            int curColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            numberBoxes.get(i).setBackgroundColor(curColor);
        }
    }

    public void ButtonClick() {

        Button buttonsbm = (Button)findViewById(R.id.binNext);
        final TextView txt = (TextView)findViewById(R.id.n4);

        final ImageView bot1 = (ImageView)findViewById(R.id.botArrow1);
        final ImageView bot2 = (ImageView)findViewById(R.id.botArrow2);
        final ImageView bot3 = (ImageView)findViewById(R.id.botArrow3);

        final TextView binText = (TextView)findViewById(R.id.binText);
        final TextView topText = (TextView)findViewById(R.id.topText);
        final TextView congrats = (TextView)findViewById(R.id.congrats);

        final TextView sec = (TextView)findViewById(R.id.n5);
        final ImageView div1 = (ImageView)findViewById(R.id.div1);
        final ImageView div2 = (ImageView)findViewById(R.id.div2);

        buttonsbm.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(cnt == 0 && !flag) {
                            binText.setText(getString(R.string.b0));
                            topText.setVisibility(View.VISIBLE);
                            flag = true;
                            return;
                        }
                        if(cnt == 0) {
                            bot1.setVisibility(View.VISIBLE);
                            binText.setText(getString(R.string.b1));
                        }
                        else if(cnt == 1) {
                            div1.setVisibility(View.VISIBLE);
                            binText.setText(getString(R.string.b2));
                        } else if(cnt == 2) {
                            binText.setText(getString(R.string.b3));
                            TextView[] toColor = {(TextView)findViewById(R.id.n1), (TextView)findViewById(R.id.n2), (TextView)findViewById(R.id.n3), (TextView)findViewById(R.id.n4)};
                            for(int i = 0; i < toColor.length; i++) {
                                toColor[i].setBackgroundColor(Color.GRAY);
                            }
                        } else if(cnt == 3) {
                            binText.setText(getString(R.string.b4));
                            bot1.setVisibility(View.INVISIBLE);
                            div1.setVisibility(View.INVISIBLE);
                            bot2.setVisibility(View.VISIBLE);
                        } else if(cnt == 4) {
                            binText.setText(getString(R.string.b5));
                            div2.setVisibility(View.VISIBLE);
                            cnt++;
                            return;
                        } else if(cnt == 5) {
                            binText.setText(getString(R.string.b6));
                            TextView[] toColor = {(TextView)findViewById(R.id.n6), (TextView)findViewById(R.id.n7)};
                            for(int i = 0; i < toColor.length; i++) {
                                toColor[i].setBackgroundColor(Color.GRAY);
                            }
                        } else if(cnt == 6) {
                            topText.setVisibility(View.INVISIBLE);
                            congrats.setTextColor(Color.GREEN);
                            congrats.setVisibility(View.VISIBLE);

                            binText.setText(getString(R.string.b7));
                            div2.setVisibility(View.INVISIBLE);
                            bot2.setVisibility(View.INVISIBLE);
                            bot3.setVisibility(View.VISIBLE);
                            TextView t1 = (TextView) findViewById(R.id.n5);
                            t1.setBackgroundColor(Color.GREEN);
                        }
                        cnt++;
                    }
                }
        );
    }

}
