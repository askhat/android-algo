package com.example.askhat.training;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class TestKnowledge extends AppCompatActivity {

    private ImageView imageView;
    private Map<RadioButton, Boolean> buttonState;
    private Map<String, String> answers;
    private List<String> allUrls;

    private List<RadioButton> buttons;
    private List<String> variants;
    private int activeUrl = 0;
    public static int score = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_knowledge);

        initialize();
        changeQuestion();

        for(int i = 0; i < buttons.size(); i++) {
            buttonState.put(buttons.get(i), false);
            ButtonClick(buttons.get(i));
        }

    }



    public void ButtonClick(final RadioButton buttonsbm) {
        buttonsbm.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        buttonsbm.setChecked(!buttonState.get(buttonsbm));
                        buttonState.put(buttonsbm, !buttonState.get(buttonsbm));
                    }
                }
        );
    }

    private void showToast(int duration, final String message) {
        final Toast toast = Toast.makeText(getBaseContext(),
                message,
                Toast.LENGTH_SHORT);
        toast.show();
        new CountDownTimer(duration, 500) {
            public void onTick(long millisUntilFinished) {
                toast.show();showToast(500, message);
            }
            public void onFinish() {
                toast.cancel();
            }

        }.start();
    }

    public void changeQuestion() {
        imageView = (ImageView)findViewById(R.id.imageView);
        if(activeUrl < allUrls.size()) {
            Glide.with(getApplicationContext()).load(allUrls.get(activeUrl)).into(imageView);
            Random ran = new Random();

            int rightAnswerIndex = ran.nextInt(buttons.size());
            buttons.get(rightAnswerIndex).setText(answers.get(allUrls.get(activeUrl)));

            for(int i = 0; i < buttons.size(); i++) {
                if(i != rightAnswerIndex) {
                    int possibleIndex = ran.nextInt(variants.size());
                    while (variants.get(possibleIndex).equals(answers.get(allUrls.get(activeUrl)))) {
                        possibleIndex = ran.nextInt(variants.size());
                    }
                    buttons.get(i).setText(variants.get(possibleIndex));
                }
            }

        } else {
            //show end dialogue

            Intent intent = new Intent(this, Congrats.class);
            startActivity(intent);
        }

    }

    public void unCheckButtons() {
        for(int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setChecked(false);
            buttonState.put(buttons.get(i), false);
        }
    }

    public void SubmitButton(View view) {

        int activeCnt = 0;
        boolean flag = false;
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).isChecked()) {
                activeCnt++;
            }
        }
        if (activeCnt >= 2) {
            showToast(500, "You should choose only one answer!");
            flag = true;
        } else if (activeCnt == 0) {
            showToast(500, "Please, choose one possible answer!");
            flag = true;
        }

        if (flag) {
            unCheckButtons();
        } else {
            for(int i = 0; i < buttons.size(); i++) {
                if(buttons.get(i).isChecked()) {
                    String curText = buttons.get(i).getText().toString();
                    String result = answers.get(allUrls.get(activeUrl));

                    if(result.equals(curText)) {
                        showToast(1000, "YOU ARE RIGHT. GREAT!:) +7 points");
                        activeUrl++;

                        buttons.get(i).setChecked(false);
                        buttonState.put(buttons.get(i), false);
                        score += 7;

                        changeQuestion();

                        return;
                    } else {
                        showToast(1000, "Please, try again. -1 point");
                        score -=1;
                        unCheckButtons();
                    }
                }
            }

        }
    }

    public void initialize() {
        allUrls = new ArrayList<String>();
        variants = new ArrayList<String>();
        buttons = new ArrayList<RadioButton>();
        answers = new HashMap<>();
        buttonState = new HashMap<>();


        variants.add("O(N * N)");
        variants.add("O(N * log N)");
        variants.add("O(N * log(log(N)))");
        variants.add("O(N)");
        variants.add("O(N * Sqrt(N))");
        variants.add("2^n");
        variants.add("n!");
        variants.add("2^(n!)");
        variants.add("Θ(sqrt(n))");
        variants.add("Θ(loglogn)");
        variants.add("Θ(logn)");
        variants.add("O(n^2Logn)");
        variants.add("O(log(log(log(N)))");
        variants.add("Θ(n^2 / Logn)");
        variants.add("Θ(n)");
        variants.add("Θ(nLogn)");
        variants.add("Θ(n^2)");
        variants.add("O(log N)");
        variants.add("O(N / 2)");
        variants.add("O(N + M) time, O(N + M) space");
        variants.add("O(N + M) time, O(1) space");
        variants.add("O(N * M) time, O(N * M) space");
        variants.add("O(N * N) time, O(1) space");
        variants.add("Can't say. Depends on the value of the array");

        String url1 = "https://firebasestorage.googleapis.com/v0/b/training-51b12.appspot.com/o/Screenshot%20from%202017-12-13%2003-06-19.png?alt=media&token=24ca1f8a-ed6d-4f49-89c9-0003ad864b43";
        String url2 = "https://firebasestorage.googleapis.com/v0/b/training-51b12.appspot.com/o/Screenshot%20from%202017-12-13%2004-25-12.png?alt=media&token=43040b35-285d-4d24-bfed-fef77c349816";
        String url3 = "https://firebasestorage.googleapis.com/v0/b/training-51b12.appspot.com/o/Screenshot%20from%202017-12-13%2006-11-41.png?alt=media&token=a7ef8ae5-6050-4943-aa07-22406e80bcf2";
        String url4 = "https://firebasestorage.googleapis.com/v0/b/training-51b12.appspot.com/o/Screenshot%20from%202017-12-13%2006-18-12.png?alt=media&token=5f14d352-f32f-4f21-9652-458e5d7b992e";
        String url5 = "https://firebasestorage.googleapis.com/v0/b/training-51b12.appspot.com/o/Screenshot%20from%202017-12-13%2006-25-28.png?alt=media&token=6296ee86-35c7-4fff-ac32-83b72e2211ea";
        String url6 = "https://firebasestorage.googleapis.com/v0/b/training-51b12.appspot.com/o/Screenshot%20from%202017-12-13%2006-29-38.png?alt=media&token=b3220b3b-bce2-46fb-b06f-dda000b9674e";


        allUrls.add(url1);
        allUrls.add(url2);
        allUrls.add(url3);
        allUrls.add(url4);
        allUrls.add(url5);
        allUrls.add(url6);


        answers.put(url1, "O(log N)");
        answers.put(url2, "O(N)");
        answers.put(url3, "O(N + M) time, O(1) space");
        answers.put(url4, "O(N * N) time, O(1) space");
        answers.put(url5, "O(N*N)");
        answers.put(url6, "O(N)");


        buttons.add((RadioButton)findViewById(R.id.choice1));
        buttons.add((RadioButton)findViewById(R.id.choice2));
        buttons.add((RadioButton)findViewById(R.id.choice3));
        buttons.add((RadioButton)findViewById(R.id.choice4));

        Collections.shuffle(allUrls);

    }


}
