package com.example.askhat.training;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class DisplayLinearSearchActivity extends AppCompatActivity {

    private int changeX = 200;
    private int cnt = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_linear_search);
        ButtonClick();

    }

    public void ButtonClick() {
        final ImageView img = (ImageView)findViewById(R.id.arrowImg);
        final TextView txt = (TextView)findViewById(R.id.lstext);

        Button buttonsbm = (Button)findViewById(R.id.binNext);

        buttonsbm.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(cnt < 3) {
                            img.setX(changeX);
                            changeX += 175;
                            cnt++;
                        } else {
                            final EditText number = (EditText)findViewById(R.id.findValue);
                            number.setBackgroundColor(Color.GREEN);
                            txt.setText(getString(R.string.linearEnd));
                        }
                    }
                }
        );
    }
}
