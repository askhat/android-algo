package com.example.askhat.training;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void algoMenu(View view) {
        Intent intent = new Intent(this, Algorithms.class);
        startActivity(intent);
    }

    public void testKnowledge(View view) {
        Intent intent = new Intent(this, TestKnowledge.class);
        startActivity(intent);
    }
}
